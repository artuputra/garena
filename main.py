from flask import Flask,jsonify,request, render_template,flash,redirect,url_for,abort,session, make_response
from datetime import datetime,timedelta 
from functools import wraps
from passlib.hash import sha256_crypt
from os import listdir
from os.path import isfile, join
from flask_sqlalchemy import SQLAlchemy 

from view.models.Models import Users

import logging, os, sys, traceback,os.path,gc,random,datetime as dt,requests, json,hashlib

app = Flask(__name__, static_url_path='/static')
app.config['SECRET_KEY'] = 'vnkdjnfjknfl1232#'

dbhost = 'localhost'
dbuser = 'semesta'
dbpass = 'password'
dbname = 'garena'
DB_URI = 'mysql://' + dbuser + ':' + dbpass + '@' + dbhost + '/' +dbname
app.config['SQLALCHEMY_DATABASE_URI'] =  DB_URI
db = SQLAlchemy(app)

# port = 443
port = 8001

date_time_now = datetime.now().strftime('%d/%m/%Y - %H:%M:%S')
date_now = datetime.now().strftime('%d/%b/%Y')
date_referal = datetime.now().strftime('%d%m%y%H%M')
time_now = datetime.now().strftime('%H:%M')


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('login'))
    return wrap

def create_new_folder(local_dir):
    newpath = local_dir
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    return newpath

@app.route('/registration', methods=["POST"])
def registration():
    data = request.json
    for i in data:
        if len(data[str(i)])==0:
            if len(data["referal_code"])==0:
                pass
            else:
                status=False
                note="Missing "+i+" field, invalid field information."
                return make_response(jsonify(status=status,note=note),200)
        else:
            pass

    status=True
    note="Success to register"
    username = data["username"]
    password = data["password"]
    password = hashlib.sha256(password.encode('utf-8')).hexdigest()
    name = data["name"]
    email = data["email"]
    referal = name+date_referal

    regis = Users(
                    username=username,
                    password=password,
                    name=name,
                    email=email,
                    referral=referal
                )
    db.session.add(regis)
    db.session.commit()
    return make_response(jsonify(status=status,note=note),200)

@app.route('/login', methods=["POST"])
def login():
    data = request.json
    username = data["username"]
    password = data["password"]
    password = hashlib.sha256(password.encode('utf-8')).hexdigest()
    user_data = Users.query.filter_by(username=username).all()
    password_db = Users.query.with_entities(Users.password).all()
    password_db = sha256_crypt.encrypt(password_db[0][0])
    if sha256_crypt.verify(password,password_db):
        print(True)
    else:
        print(False)
    return make_response(jsonify(status="oke"),200)

@app.route('/edit', methods=["POST"])
def edit():
    data = request.json
    username = data["username"]
    email = data["email"]
    db.session.query(Users).filter_by(username=username).update({"email":email})
    db.session.commit()
    return make_response(jsonify(status="Success edit data"),200)

@app.route('/refcode', methods=["POST"])
def refcode():
    data = request.json
    username = data['username']
    ref_code = data["ref_code"]
    if not ref_code:
        return make_response(jsonify(status="Referal code doesn't exist"),200)
    else:
        referal = Users.query.with_entities(Users.referral).filter_by(referral=ref_code,username=username).all()    
        if not referal:
            db.session.query(Users).filter_by(username=username).update({"referral_from":ref_code})
            db.session.commit()
            return make_response(jsonify(status="success input"),200)
        else:
            return make_response(jsonify(status="cannot insert for it self"),200)

@app.route('/find', methods=["POST"])
def find():
    data = request.json
    name = data['name']
    like_name = "%{}%".format(name)
    by_name = Users.query.filter(Users.name.like(like_name)).all()
    print(by_name)
    return make_response(jsonify(status="cannot insert for it self"),200)

if __name__ == '__main__':
    app.run(debug=True, port=port, host='0.0.0.0')

