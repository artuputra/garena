Flask==0.12.5
Flask-Migrate==2.5.3
Flask-MySQLdb==0.2.0
Flask-SQLAlchemy==2.4.1
Jinja2==2.10.3
SQLAlchemy==1.3.13