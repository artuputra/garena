from flask import Flask,jsonify
from flask_sqlalchemy import SQLAlchemy 
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from datetime import datetime, date
from hashlib import md5
import json

# from bcrypt import hashpw, gensalt

app = Flask(__name__)
dbhost = 'localhost' #Here you can change mysql host
dbuser = 'semesta'   #Here you can change mysql user
dbpass = 'password'  #Here you can change mysql password
dbname = 'garena'    #Here you can change mysql database, first of all you should creat new db
DB_URI = 'mysql://' + dbuser + ':' + dbpass + '@' + dbhost + '/' +dbname
app.config['SQLALCHEMY_DATABASE_URI'] =  DB_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True,nullable=False)
    password = db.Column(db.Text(255),nullable=False)
    name = db.Column(db.String(255),nullable=False)
    email = db.Column(db.String(255), unique=True,nullable=False)
    referral = db.Column(db.String(255), unique=True)
    referral_from = db.Column(db.String(255),default='-')

    def __repr__(self):
        # data = {"username" : self.username, 
        #         "password" : self.password, 
        #         "name" : self.name, 
        #         "email" : self.email, 
        #         "referral" : self.referral, 
        #         "referral_from" : self.referral_from}
        return "<username: {},password: {},name: {},email: {},referral: {},referral_from: {}>".format(self.username,self.password,self.name,self.email,self.referral,self.referral_from)
        # return json.dumps(data)

# class User(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     slug = db.Column(db.String(80))
#     username = db.Column(db.String(80), unique=True)
#     email = db.Column(db.String(80))
#     password = db.Column(db.String(80), unique=False)
#     admin = db.Column(db.Boolean(), default=False)
#     join_date = db.Column(db.DateTime)
#     last_seen = db.Column(db.DateTime)
#     topics = db.relationship('Topic')
#     posts = db.relationship('Post')
#     picture = db.Column(db.Boolean(), default=False)
#     title = db.Column(db.String(80))
#     company = db.Column(db.String(80))
#     summary = db.Column(db.String(80))

# class Category(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(80), unique=True)
#     description = db.Column(db.String(180), unique=False)
#     topics = db.relationship('Topic', backref="category")

# class Topic(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     slug = db.Column(db.String(255), unique=True)
#     title = db.Column(db.String(80), unique=False)
#     description = db.Column(db.Text, unique=False)
#     pub_date = db.Column(db.DateTime)
#     last_update = db.Column(db.DateTime)
#     user_id = db.Column(db.String(80), db.ForeignKey('user.id'))
#     category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
#     views = db.Column(db.Integer, default=0)
#     locked = db.Column(db.Boolean(), default=False)
#     pinned = db.Column(db.Boolean(), default=False)
#     user = db.relationship('User')
#     posts = db.relationship('Post')

if __name__ == '__main__':
    manager.run()