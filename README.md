# README #

This is the repository for garena online test

### How do I run? ###

* Move your directory into view/models using cd view/models
* Make database in your sql with name garena
* Run python Models.py db init
* Run python Models.py db migrate
* Run python Models.py db upgrade
* Bring out your directory to garena
* Run server using python main.py
* test the server using python post.py
